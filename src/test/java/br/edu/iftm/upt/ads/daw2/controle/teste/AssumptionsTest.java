/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.daw2.controle.teste;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.junit.Assume.assumeFalse;
import static org.junit.Assume.assumeNoException;
import static org.junit.Assume.assumeThat;
import static org.junit.Assume.assumeTrue;

/**
 *
 * Testes que vão rodar se determinadas condições forem satisfeitas, se não forem, irão ignorar o teste
 * 
 * @author aluno
 */
public class AssumptionsTest {
 
    @Test
    public void shouldRunOnlyOnLinux() {
        assumeThat(System.getProperty("os.name"), is("Linux"));
        assertThat(Paths.get("/tmp").toString(), equalTo("/tmp"));
    }
 
    @Test
    public void shouldRunOnlyWhenOneIsOne() {
        assumeTrue("Expected true!", 1 == 1);
        assertThat(1 + 1, is(2));
    }
 
    @Test
    public void shouldRunOnlyWhenFalse() {
        assumeFalse("Expected false!", 1 == 2);
        assertThat(Math.pow(2, 3), is(8.0));
    }
 
    @Test
    public void shouldRunOnlyWhenCanOpenFile() {
        List<String> data = null;
        try {
            data = Files.readAllLines(Paths.get("/proc/meminfo"));
        } catch (IOException e) {
            assumeNoException(e);
        }
        assertThat(data.isEmpty(), is(false));
    }
}
