/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.daw2.controle.teste;

import java.util.Collections;
import static java.util.Collections.emptyList;
import java.util.concurrent.TimeUnit;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;

/**
 *
 * @author aluno
 */
public class MoreFeaturesTest {
 
    @Ignore("Fix that tomorrow")
    @Test
    public void shouldBeFixedSoon() {
        assertEquals(4, Math.abs(-5));
    }
 
    @Test(timeout = 500)
    public void shouldDoWorkInSpecifiedTime() throws InterruptedException {
        TimeUnit.MILLISECONDS.sleep(300);
        assertEquals(2, 2);
    }
 
    @Test(expected = IndexOutOfBoundsException.class)
    public void shouldThrowException() {
        Collections.swap(emptyList(), 0, 1);
    }
}
