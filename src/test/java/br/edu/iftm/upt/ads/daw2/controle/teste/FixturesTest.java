/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.iftm.upt.ads.daw2.controle.teste;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author aluno
 */
public class FixturesTest {
 
    private User user;
    private User jon = new User("Jon", "Snow");
 
    @BeforeClass
    public static void executedOnceBeforeAllTestsInThisClass() {
        System.out.println("Instantiating a heavy resource.");
    }
 
    @Before
    public void calledBeforeEachTest() {
        System.out.println("Recreating User!");
        user = new User("James", "Brown");
    }
 
    @After
    public void calledAfterEachTest() {
        System.out.println("Cleanup after a test!");
    }
 
    @AfterClass
    public static void executedOnceAfterAllTestsInThisClass() {
        System.out.println("Disposing a heavy resource.");
    }
 
    @Test
    public void shouldHaveJonSnow() {
        System.out.println("Checking Jon Snow.");
        assertNotNull(jon);
        assertEquals("Jon", jon.getFirstName());
        assertEquals("Snow", jon.getLastName());
    }
 
    @Test
    public void shouldHaveUser() {
        System.out.println("Executing shouldHaveUser test");
        assertNotNull(user);
        assertEquals("James", user.getFirstName());
        assertEquals("Brown", user.getLastName());
 
        // this affects only current method:
        user.setFirstName("Michael");
        user.setLastName("Jackson");
    }
 
    @Test
    public void shouldHaveRecreatedUser() {
        System.out.println("Executing shouldHaveRecreatedUser test");
        assertNotNull(user);
        assertEquals("James", user.getFirstName());
        assertEquals("Brown", user.getLastName());
    }
}